# Créé par donniod.julie, le 16/02/2024 en Python 3.7
import pygame
import random
pygame.init()


import pygame
from pygame.locals import*


pygame.init()




def import_img():
    #import les img
    curseur = pygame.image.load("D:/NSI/Trophé NSI/curseur.png")
    cible_1 = pygame.image.load("D:/NSI/Trophé NSI/cible_1.png")
    cible_2 = pygame.image.load("D:/NSI/Trophé NSI/cible_2.png")
    cible_3 = pygame.image.load("D:/NSI/Trophé NSI/cible_3.png")
    cible_4 = pygame.image.load("D:/NSI/Trophé NSI/cible_4.png")
    return(curseur,cible_1,cible_2,cible_3,cible_4)


def pré_reglage(curseur):
    #création de la fenètre
    ecran = pygame.display.set_mode((800, 450))
    pygame.display.set_caption("Trophé NSI")


    #création de la flèche
    flèche_X = 800/2-5
    flèche_Y = 400/2-5+50
    curseur_position = (flèche_X, flèche_Y)
    ecran.blit(curseur,curseur_position )
    pygame.display.flip()
    return(ecran,flèche_X,flèche_Y)


#taille des cible
def taille_cible_(cible_1,cible_2,cible_3,cible_4):
    taille_cible_1_à_4 = random.randint(1,4)
    if taille_cible_1_à_4 == 1:
        cible = cible_1
        taille_cible_def = 25
        pts_gagner = 4
    if taille_cible_1_à_4 == 2:
        cible = cible_2
        taille_cible_def = 50
        pts_gagner = 3
    if taille_cible_1_à_4 == 3:
        cible = cible_3
        taille_cible_def = 75
        pts_gagner = 2
    if taille_cible_1_à_4 == 4:
       cible = cible_4
       taille_cible_def = 100
       pts_gagner = 1
    return cible,taille_cible_def,pts_gagner


#position x des cible
def position_x():
    cible_X = random.randint(100,700)
    return cible_X


#position y des cible
def position_y():
    cible_y = random.randint(150,350)
    return cible_y


def chargement(cible,position_cible,flèche_X,flèche_Y,ecran,bandeau,back,curseur):
    pygame.draw.rect(ecran,(82,167,53),bandeau)
    pygame.draw.rect(ecran,(0,0,0),back)
    ecran.blit(cible,position_cible)
    curseur_position = (flèche_X, flèche_Y)
    ecran.blit(curseur,curseur_position )
    pygame.display.flip()


def réglage1():
    #reglage
    prés_visualisation = 1
    pts = 0
    bandeau = pygame.Rect(0,0,800,50)
    back = pygame.Rect(0,50,800,400)
    jeux = True
    pv = 3
    position_texte_pv = (250, 20)
    position_texte_pts = (50, 20)
    police = pygame.font.Font(None, 36)
    return(prés_visualisation,pts,bandeau,back,jeux,pv,position_texte_pv,position_texte_pts,police)


def jeux_arc_lancement(prés_visualisation,pts,bandeau,back,jeux,pv,position_texte_pv,position_texte_pts,police,curseur,cible_1,cible_2,cible_3,cible_4,ecran,flèche_X,flèche_Y):
    #boucle du jeux
    while jeux:
        #création texte pts
        texte_pts = police.render(str(pts), True, (255, 255, 255))
        ecran.blit(texte_pts, position_texte_pts)
        #création texte pv
        texte_pv = police.render(str(pv), True, (255, 255, 255))
        ecran.blit(texte_pv, position_texte_pv)


        pygame.display.flip()
        for event in pygame.event.get():
            #création cible
            if prés_visualisation == 1 :
                #def taille et position de la cible
                cible,taille_cible_def,pts_gagner = taille_cible_(cible_1,cible_2,cible_3,cible_4)
                position_x_def = position_x()
                position_y_def = position_y()
                position_cible = (position_x_def,position_y_def)


                pygame.draw.rect(ecran,(82,167,53),bandeau)
                #création de la cible
                ecran.blit(cible,position_cible )
                #réglage
                prés_visualisation = 2
            #quitté le jeux
            if event.type == pygame.QUIT:
                jeux = False
            #mouvement de la flèche
            if event.type == pygame.KEYDOWN:


                if event.key == pygame.K_LEFT:
                    flèche_X=flèche_X-20
                    chargement(cible,position_cible,flèche_X,flèche_Y,ecran,bandeau,back,curseur)


                if event.key == pygame.K_RIGHT:
                    flèche_X=flèche_X+20
                    chargement(cible,position_cible,flèche_X,flèche_Y,ecran,bandeau,back,curseur)


                if event.key == pygame.K_UP:
                    flèche_Y=flèche_Y-20
                    chargement(cible,position_cible,flèche_X,flèche_Y,ecran,bandeau,back,curseur)


                    pygame.display.flip()
                if event.key == pygame.K_DOWN:
                    flèche_Y=flèche_Y+20
                    chargement(cible,position_cible,flèche_X,flèche_Y,ecran,bandeau,back,curseur)


            if event.type == pygame.KEYUP:


                if event.key == pygame.K_LEFT:
                    chargement(cible,position_cible,flèche_X,flèche_Y,ecran,bandeau,back,curseur)


                if event.key == pygame.K_RIGHT:
                    chargement(cible,position_cible,flèche_X,flèche_Y,ecran,bandeau,back,curseur)


                if event.key == pygame.K_UP:
                    chargement(cible,position_cible,flèche_X,flèche_Y,ecran,bandeau,back,curseur)


                if event.key == pygame.K_DOWN:
                    chargement(cible,position_cible,flèche_X,flèche_Y,ecran,bandeau,back,curseur)
                    plus_tmp = 5
                    k = 1


                #tire la flèche
                if event.key == pygame.K_SPACE:
                    if flèche_X+5 >= position_x_def and  flèche_X+5 <= position_x_def+taille_cible_def and flèche_Y+5 >= position_y_def and flèche_Y+5 <= position_y_def+taille_cible_def:
                        #pst et réglage
                        pts = pts + pts_gagner
                        prés_visualisation = 1
                        pygame.draw.rect(ecran,(82,167,53),bandeau)
                        pygame.draw.rect(ecran,(0,0,0),back)
                        curseur_position = (flèche_X, flèche_Y)
                        ecran.blit(curseur,curseur_position )
                        pygame.display.flip()


                    else :
                        pv = pv-1
                        chargement(cible,position_cible,flèche_X,flèche_Y,ecran,bandeau,back,curseur)
        if pv == 0 :
            pygame.draw.rect(ecran,(0,0,0),back)
            texte = police.render('Game Over', True, (255, 0, 0))
            position_texte = (300, 200)
            ecran.blit(texte, position_texte)


        if pv == -1 :
            pygame.draw.rect(ecran,(0,0,0),back)
            pygame.display.flip()
            jeux = False


        if pts >= 50 :
            achive = 1
            if pts <= 100 :
                achive = 2


def jeux_arc():
    curseur,cible_1,cible_2,cible_3,cible_4 = import_img()
    prés_visualisation,pts,bandeau,back,jeux,pv,position_texte_pv,position_texte_pts,police = réglage1()
    ecran,flèche_X,flèche_Y = pré_reglage(curseur)
    jeux_arc_lancement(prés_visualisation,pts,bandeau,back,jeux,pv,position_texte_pv,position_texte_pts,police,curseur,cible_1,cible_2,cible_3,cible_4,ecran,flèche_X,flèche_Y)


pygame.quit()


