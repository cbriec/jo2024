import pygame
pygame.init()

def info_pantheon():
    global medaille1
    global medaille2
    global medaille3
    global medaille4
    global medaille5
    global medaille6
#initialisation
    x_ecran = 800
    y_ecran = 450
    ractangle_joué_x = int(x_ecran/8)
    ractangle_joué_y = int(x_ecran/16)
    WHITE = (255, 255, 255)
    BLACK = (0, 0, 0)
    RED = (255, 0, 0)
    GREEN = (0, 255, 0)

# Font
    font = pygame.font.Font(None, 46)

#ecran
    ecran = pygame.display.set_mode((x_ecran, y_ecran))
    pygame.display.set_caption("Trophé NSI")

    ecran.fill((82,167,53))

#position titre
#titre
    titre = pygame.Rect(0,0,x_ecran,100)
    pygame.draw.rect(ecran,BLACK,titre)
    pantheon = font.render("PANTHEON", True,WHITE)
    rect_texte = pantheon.get_rect(center=titre.center)
    ecran.blit(pantheon, rect_texte)

#medaille1
    medaille1 = pygame.Rect((x_ecran*3/4)-(ractangle_joué_x/2),(y_ecran/2)-(ractangle_joué_y*2),(x_ecran)/8,ractangle_joué_y)
    pygame.draw.rect(ecran, WHITE,medaille1)
    texte_jeu = font.render("1", True,BLACK)
    rect_jeu = texte_jeu.get_rect(center=medaille1.center)
    ecran.blit(texte_jeu, rect_jeu)

#medaille2
    medaille2 = pygame.Rect((x_ecran*3/4)-(ractangle_joué_x/2),(y_ecran/2),ractangle_joué_x,ractangle_joué_y)
    pygame.draw.rect(ecran,WHITE,medaille2)
    texte_pantheon = font.render("2", True,BLACK)
    rect_pantheon = texte_pantheon.get_rect(center=medaille2.center)
    ecran.blit(texte_pantheon, rect_pantheon)

#medaille3
    medaille3 = pygame.Rect(((x_ecran/2))-(ractangle_joué_x/2),(y_ecran/2)-(ractangle_joué_y*2),(x_ecran)/8,ractangle_joué_y)
    pygame.draw.rect(ecran,WHITE,medaille3)
    texte_pantheon = font.render("3", True,BLACK)
    rect_pantheon = texte_pantheon.get_rect(center=medaille3.center)
    ecran.blit(texte_pantheon, rect_pantheon)

    #medaille4
    medaille4 = pygame.Rect(((x_ecran/2))-(ractangle_joué_x/2),(y_ecran/2),ractangle_joué_x,ractangle_joué_y)
    pygame.draw.rect(ecran,WHITE,medaille4)
    texte_pantheon = font.render("4", True,BLACK)
    rect_pantheon = texte_pantheon.get_rect(center=medaille4.center)
    ecran.blit(texte_pantheon, rect_pantheon)

    #medaille5
    medaille5 = pygame.Rect((x_ecran/4)-(ractangle_joué_x/2),(y_ecran/2)-(ractangle_joué_y*2),(x_ecran)/8,ractangle_joué_y)
    pygame.draw.rect(ecran,WHITE,medaille5)
    texte_pantheon = font.render("5", True,BLACK)
    rect_pantheon = texte_pantheon.get_rect(center=medaille5.center)
    ecran.blit(texte_pantheon, rect_pantheon)

    #medaille6
    medaille6 = pygame.Rect((x_ecran/4)-(ractangle_joué_x/2),(y_ecran/2),ractangle_joué_x,ractangle_joué_y)
    pygame.draw.rect(ecran,WHITE,medaille6)
    texte_pantheon = font.render("6", True,BLACK)
    rect_pantheon = texte_pantheon.get_rect(center=medaille6.center)
    ecran.blit(texte_pantheon, rect_pantheon)


#bouton retour
    retour = pygame.Rect((x_ecran*3/4)-(ractangle_joué_x/2),(y_ecran/4)-(ractangle_joué_y*2),(x_ecran)/8,ractangle_joué_y)
    pygame.draw.rect(ecran,WHITE,retour)
    texte_pantheon = font.render("retour", True,BLACK)
    rect_pantheon = texte_pantheon.get_rect(center=retour.center)
    ecran.blit(texte_pantheon, rect_pantheon)


    pygame.display.flip()


def retour_page_avant():
    page()

def ouverture_p(retour):
    global medaille1
    global medaille2
    global medaille3
    info_pantheon()
    jeux = True
    while jeux:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                jeux = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if medaille1.collidepoint(event.pos):
                    print("lol")
                elif medaille2.collidepoint(event.pos):
                    print("lo")
                elif medaille3.collidepoint(event.pos):
                    print("l")
                elif retour.collidepoint(event.pos):
                    return
    pygame.quit()



