import pygame
from arc import jeux_arc
from pantheon import ouverture_p
pygame.init()



#initialisation
x_ecran = 800
y_ecran = 450
ractangle_joué_x = int(x_ecran/8)
ractangle_joué_y = int(x_ecran/16)
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)

# Font
font = pygame.font.Font(None, 46)

#ecran
ecran = pygame.display.set_mode((x_ecran, y_ecran))
pygame.display.set_caption("Trophé NSI")

ecran.fill((82,167,53))

# Définir la variable retour
retour = pygame.Rect((x_ecran - ractangle_joué_x) / 2, 5, ractangle_joué_x, ractangle_joué_y)

#position titre
titre_jeu = pygame.Rect((x_ecran/2)-(ractangle_joué_x*2),(y_ecran/4)-(ractangle_joué_y/2),ractangle_joué_x*4,ractangle_joué_y*1.5)
pygame.draw.rect(ecran,BLACK,titre_jeu)
texte_titre = font.render("Tir à l'arc", True,WHITE)
rect_texte = texte_titre.get_rect(center=titre_jeu.center)
ecran.blit(texte_titre, rect_texte)

# Position initiale des boutons
#bouton jeu
rectangle_joué = pygame.Rect(((x_ecran/2))-(ractangle_joué_x/2),(y_ecran/2)-(ractangle_joué_y/2),ractangle_joué_x,ractangle_joué_y)
pygame.draw.rect(ecran, WHITE,rectangle_joué)
texte_jeu = font.render("Jouer", True,BLACK)
rect_jeu = texte_jeu.get_rect(center=rectangle_joué.center)
ecran.blit(texte_jeu, rect_jeu)

#bouton pantheon
rectangle_pantheon = pygame.Rect(((x_ecran/2))-(ractangle_joué_x/2),(y_ecran)-(ractangle_joué_y*3),ractangle_joué_x,ractangle_joué_y)
pygame.draw.rect(ecran,WHITE,rectangle_pantheon)
texte_pantheon = font.render("pantheon", True,BLACK)
rect_pantheon = texte_pantheon.get_rect(center=rectangle_pantheon.center)
ecran.blit(texte_pantheon, rect_pantheon)


# Fonction pour démarrer le jeu de tir à l'arc
def start_game():
    print("Le jeu de tir à l'arc commence !")
    jeux_arc()

# Fonction pour afficher le panthéon


pygame.display.flip()
def page():
    jeux = True
    while jeux:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                jeux = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if rectangle_joué.collidepoint(event.pos):
                    start_game()
                elif rectangle_pantheon.collidepoint(event.pos):
                    ouverture_p(retour)
    pygame.quit()


page()



